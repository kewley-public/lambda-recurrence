package com.kewley.aws.lambda.recurrence.service

import com.kewley.aws.lambda.recurrence.dto.RecurrenceRequestDTO
import com.kewley.aws.lambda.recurrence.dto.RecurringEventDTO
import org.dmfs.rfc5545.DateTime
import spock.lang.Specification
import spock.lang.Unroll

import java.time.OffsetDateTime
import java.time.ZoneOffset

class RecurrenceRuleServiceSpec extends Specification {

    final static DateTime NOW = new DateTime(2018, 7, 26)
    final static String TIMEZONE = 'America/Chicago'
    final static ZoneOffset OFFSET = ZoneOffset.of('-05:00')
    NowService nowService = Mock(NowService)
    RecurrenceRuleService service = new RecurrenceRuleService(
            8,
            nowService
    )

    @Unroll
    void 'generateRecurringEventsFromRequest: with rule: #rule, dateStart: #dateStartRule, dateEnd: #dateEndRule'() {
        when:
        List<RecurringEventDTO> events = service.generateRecurringEventsFromRequest(RecurrenceRequestDTO
                .builder()
                .dateStartRule(dateStartRule)
                .dateEndRule(dateEndRule)
                .recurrenceRule(rule)
                .timezone(TIMEZONE)
                .build())

        then:
        (0..1) * nowService.determineNow(_) >> NOW
        0 * _

        expected == events

        where:
        rule                                                     | dateStartRule                            | dateEndRule                            | expected
        'FREQ=WEEKLY;INTERVAL=1;BYDAY=MO;COUNT=5'                | generateDateStartRule('20180825T094500') | generateDateEndRule('20180825T113000') | expectedWeeklyCase()
        'FREQ=WEEKLY;INTERVAL=1;BYDAY=MO;UNTIL=20180901T000000Z' | generateDateStartRule('20180825T094500') | generateDateEndRule('20180825T113000') | expectedUntilCase()
        'FREQ=MONTHLY;BYDAY=TH;BYSETPOS=5;COUNT=3'               | generateDateStartRule('20180825T094500') | generateDateEndRule('20180825T113000') | expectedMonthlyCase()
        'FREQ=WEEKLY;INTERVAL=1;WKST=MO;BYDAY=MO,TU;COUNT=4'     | null                                     | null                                   | expectedNoDateStart()
    }

    private static List<RecurringEventDTO> expectedWeeklyCase() {
        return [
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 8, 27, 9, 45, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 8, 27, 11, 30, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 9, 3, 9, 45, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 9, 3, 11, 30, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 9, 10, 9, 45, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 9, 10, 11, 30, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 9, 17, 9, 45, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 9, 17, 11, 30, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 9, 24, 9, 45, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 9, 24, 11, 30, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
        ]
    }

    private static List<RecurringEventDTO> expectedNoDateStart() {
        return [
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 8, 26, 0, 0, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 8, 26, 0, 0, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 8, 27, 0, 0, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 8, 27, 0, 0, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 9, 2, 0, 0, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 9, 2, 0, 0, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 9, 3, 0, 0, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 9, 3, 0, 0, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
        ]
    }

    private static List<RecurringEventDTO> expectedUntilCase() {
        return [
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 8, 27, 9, 45, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 8, 27, 11, 30, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
        ]
    }

    private static List<RecurringEventDTO> expectedMonthlyCase() {
        return [
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 8, 30, 9, 45, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 8, 30, 11, 30, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2018, 11, 29, 9, 45, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2018, 11, 29, 11, 30, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
                RecurringEventDTO.builder()
                        .startTimeMillis(OffsetDateTime.of(2019, 1, 31, 9, 45, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .endTimeMillis(OffsetDateTime.of(2019, 1, 31, 11, 30, 0, 0, OFFSET).toInstant().toEpochMilli())
                        .build(),
        ]
    }

    private static String generateDateStartRule(String dateAsString) {
        return "DTSTART=${dateAsString}"
    }

    private static String generateDateEndRule(String dateAsString) {
        return "DTEND=${dateAsString}"
    }

}
