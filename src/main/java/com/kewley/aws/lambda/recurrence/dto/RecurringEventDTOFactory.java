package com.kewley.aws.lambda.recurrence.dto;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public class RecurringEventDTOFactory {

    /**
     * Will create an {@link RecurringEventDTO} from the provided {@code millis}
     *
     * @param millis     - The time stamp
     * @param startTime  - The start time to be used for the {@link RecurringEventDTO} (e.g. 09:00)
     * @param endTime    - The end time to be used for the {@link RecurringEventDTO} (e.g. 11:00)
     * @param zoneOffset - The zone offset for the {@link RecurringEventDTO}
     * @return {@link RecurringEventDTO} the recurring event constructed from the provide parameters
     */
    public static RecurringEventDTO createRecurringEventDTOFromMillisAndTime(Long millis,
                                                                             LocalTime startTime,
                                                                             LocalTime endTime,
                                                                             ZoneOffset zoneOffset) {
        LocalDate nextDay = Instant.ofEpochMilli(millis).atZone(zoneOffset).toLocalDate();
        OffsetDateTime start = OffsetDateTime.of(nextDay, startTime, zoneOffset);
        OffsetDateTime end = OffsetDateTime.of(nextDay, endTime, zoneOffset);
        return RecurringEventDTO.builder()
                .startTimeMillis(start.toInstant().toEpochMilli())
                .endTimeMillis(end.toInstant().toEpochMilli())
                .build();
    }

}
