package com.kewley.aws.lambda.recurrence;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.kewley.aws.lambda.recurrence.dto.RecurrenceRequestDTO;
import com.kewley.aws.lambda.recurrence.dto.RecurringEventDTO;
import com.kewley.aws.lambda.recurrence.service.NowService;
import com.kewley.aws.lambda.recurrence.service.RecurrenceRuleService;

import java.util.List;
import java.util.UUID;

/**
 * Handles lambda requests
 */
public class RecurrenceHandler implements RequestHandler<RecurrenceRequestDTO, List<RecurringEventDTO>> {
    RecurrenceRuleService recurrenceRuleService = new RecurrenceRuleService(100, new NowService());

    /**
     * Lambda handler for {@link RecurrenceRequestDTO}s
     * @param request - A recurrence request
     * @param context - The lambda context
     * @return {@link List<RecurringEventDTO>} The events generated from the {@code request}
     */
    @Override
    public List<RecurringEventDTO> handleRequest(RecurrenceRequestDTO request, Context context) {
        // generate the events
        List<RecurringEventDTO> recurringEvents = recurrenceRuleService.generateRecurringEventsFromRequest(request);

        // uniquely identify the events for the client
        recurringEvents.forEach(event -> event.setId(UUID.randomUUID().toString()));

        return recurringEvents;
    }

}
