package com.kewley.aws.lambda.recurrence.service;

import org.dmfs.rfc5545.DateTime;

import java.time.ZoneId;
import java.util.TimeZone;

/**
 * Abstracted service for testing purposes only to control the
 * current time stamp.
 */
public class NowService {

    /**
     * Retrieves the current time stamp
     *
     * @param zoneId - The zone of where the current timestamp is offset to.
     * @return The current time.
     */
    public DateTime determineNow(ZoneId zoneId) {
        return DateTime.now(TimeZone.getTimeZone(zoneId));
    }

}
